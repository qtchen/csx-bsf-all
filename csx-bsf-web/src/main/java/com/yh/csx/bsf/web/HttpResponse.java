package com.yh.csx.bsf.web;
import lombok.Data;

@Data
public class HttpResponse<T> {
    protected int code;
    protected String message;
    protected T data;

    public HttpResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
}
