package com.yh.csx.bsf.file.impl;

import com.yh.csx.bsf.file.FileProvider;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author huojuncheng
 */
public abstract class AbstractFileProvider implements FileProvider {
    private static final String IMAGES = "images/", DOCS = "docs/", EXCELS = "excels/", FILES = "files/", TEMP = "temp/";
    private static Map<String, String> defaultPrefix = new HashMap<>();

    static {
        defaultPrefix.put("jpg", IMAGES);
        defaultPrefix.put("png", IMAGES);
        defaultPrefix.put("jpeg", IMAGES);
        defaultPrefix.put("gif", IMAGES);
        defaultPrefix.put("bmp", IMAGES);
        defaultPrefix.put("doc", DOCS);
        defaultPrefix.put("docx", DOCS);
        defaultPrefix.put("pdf", DOCS);
        defaultPrefix.put("txt", DOCS);
        defaultPrefix.put("xlsx", EXCELS);
        defaultPrefix.put("xls", EXCELS);
        defaultPrefix.put("csv", EXCELS);
        defaultPrefix.put("tmp", TEMP);
        defaultPrefix.put("temp", TEMP);
    }

    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
    /**
     *@描述 生成云路径
     *@参数  [path, name]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    protected String createFileKey(String path, String name) {
        String suffix = null;
        if (name != null && name.lastIndexOf('.') >= 0) {
            suffix = name.substring(name.lastIndexOf('.') + 1).trim().toLowerCase();
        }
        if (path != null) {
            while (path.startsWith("/")) {
                path = path.substring(1);
            }
        }
        if (path == null || (path = path.trim()).length() == 0) {
            path = suffix == null ? FILES : defaultPrefix.getOrDefault(suffix, FILES);
        }
        StringBuilder builder = new StringBuilder(path);
        if (!path.endsWith("/")) {
            builder.append("/");
        }
        builder.append(dateTimeFormatter.format(LocalDateTime.now())).append("/");
        builder.append(randomName());
        if (suffix != null && suffix.length() > 0) {
            builder.append('.').append(suffix);
        }
        return builder.toString();
    }
    
    private String randomName() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }
    /**
     *@描述 上传具有图片处理的文件方法
     *@参数  [filePath, name, isTemp]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25 
     *@修改历史：
     */
    @Override
    public String uploadProcessImage(String filePath,boolean isTemp) {
        return FileProviderMonitor.hook().run("uploadProcessImage", () -> uploadProcessImage(filePath, isTemp?TEMP:null));
    }
    /**
     *@描述 上传文件
     *@参数  [filePath, name, isTemp]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25 
     *@修改历史：
     */
    @Override
    public String uploadFile(String filePath,boolean isTemp) {
        return FileProviderMonitor.hook().run("uploadFile", () -> uploadFile(filePath, isTemp?TEMP:null));
    }


    public abstract String uploadProcessImage(String filePath, String path) ;

    public abstract String uploadFile(String filePath, String path);
    

}
