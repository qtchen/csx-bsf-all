package com.yh.csx.bsf.file.config;

import com.yh.csx.bsf.core.util.ReflectionUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author huojuncheng
 */
@Data
@ConfigurationProperties(prefix = "bsf.file")
public class FileProperties {
    public static final String PROVIDER_TENCENT = "腾讯云";
    public static final String PROVIDER_QINIU = "七牛云";

    public static String Project="File";
    public static String Prefix="bsf.file.";

    @Value("{$bsf.env:}")
    private String bsfEnv;

    @NestedConfigurationProperty
    private QiniuProperties qiniu;

    @NestedConfigurationProperty
    private TencentProperties tencent;

    public QiniuProperties getDefaultQiniu() {
        QiniuProperties properties = new QiniuProperties();
        properties.setAccessKey(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","qiniu_accesskey",""));
        properties.setSecurityKey(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","qiniu_securitykey",""));
        properties.setBucketName(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","qiniu_bucketname",""));
        properties.setBucketUrl(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","qiniu_bucketurl",""));
        return properties;
    }

    public TencentProperties getDefaultTencent() {
        TencentProperties properties = new TencentProperties();
        properties.setAccessKey(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","tencent_accesskey",""));
        properties.setSecurityKey(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","tencent_securitykey",""));
        properties.setBucketName(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","tencent_bucketname",""));
        properties.setBucketUrl(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","tencent_bucketurl",""));
        properties.setArea(ReflectionUtils.tryGetStaticFieldValue("com.yh.csx.bsf.core.base.BsfBaseConfig","tencent_area",""));
        return properties;
    }
}
