package com.yh.csx.bsf.file.impl;

import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.storage.persistent.FileRecorder;
import com.qiniu.util.Auth;
import com.qiniu.util.StringUtils;
import com.yh.csx.bsf.core.serialize.JsonSerializer;
import com.yh.csx.bsf.core.util.LogUtils;
import com.yh.csx.bsf.file.FileException;
import com.yh.csx.bsf.file.config.FileProperties;
import com.yh.csx.bsf.file.config.QiniuProperties;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author huojuncheng
 */
public class QiniuFileProvider extends BaseFileProvider {

    private Auth auth;
    private String bucketName;
    private String bucketUrl;
    private UploadManager uploadManager;
    private BucketManager bucketManager;

    public QiniuFileProvider(QiniuProperties properties) {
        super();
        if (StringUtils.isNullOrEmpty(properties.getAccessKey())) {
            throw new FileException("七牛文件服务缺少accessKey配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getSecurityKey())) {
            throw new FileException("七牛文件服务缺少securityKey配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getBucketName())) {
            throw new FileException("七牛文件服务缺少bucketName配置");
        }
        if (StringUtils.isNullOrEmpty(properties.getBucketUrl())) {
            throw new FileException("七牛文件服务缺少bucketUrl配置");
        }
        this.auth = Auth.create(properties.getAccessKey(), properties.getSecurityKey());
        Configuration configuration = new Configuration(Zone.autoZone());
        String tempDir = properties.getTempDir();
        if (StringUtils.isNullOrEmpty(tempDir)) {
            tempDir = "tmp/";
        }
        try {
            this.uploadManager = new UploadManager(configuration, new FileRecorder(tempDir));
        } catch (IOException e) {
            throw new FileException("创建七牛文件临时目录失败：" + tempDir, e);
        }
        this.bucketManager = new BucketManager(this.auth, configuration);
        this.bucketName = properties.getBucketName();
        String bucketUrl = properties.getBucketUrl();
        if (!bucketUrl.endsWith("/")) {
            bucketUrl += "/";
        }
        this.bucketUrl = bucketUrl;
    }
    @Deprecated
    public String upload(InputStream input, String path, String name) throws Exception {
        String token = auth.uploadToken(bucketName);
        Response response = uploadManager.put(input, createFileKey(path, name), token, null, null);
        DefaultPutRet putRet = new JsonSerializer().deserialize(response.bodyString(), DefaultPutRet.class);
        return generatePresignedUrl(putRet);
    }
    /**
     *@描述 上传七牛云文件
     *@参数  [filePath, path, name]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    public String upload(String filePath, String path) throws Exception {
         String token = auth.uploadToken(bucketName);
         Response response = uploadManager.put(filePath, createFileKey(path, filePath), token);
         DefaultPutRet putRet = new JsonSerializer().deserialize(response.bodyString(), DefaultPutRet.class);
         return generatePresignedUrl(putRet);
    }
    /**
     *@描述 获得地址
     *@参数  [putRet]
     *@返回值  java.lang.String
     *@创建人  霍钧城
     *@创建时间  2020/12/25
     *@修改历史：
     */
    private String generatePresignedUrl(DefaultPutRet putRet){
        String url=bucketUrl + putRet.key;
        LogUtils.info(TencentFileProvider.class, FileProperties.PROVIDER_QINIU,url);
        return url;
    }
}
