package com.yh.csx.bsf.bigdata;

import com.alibaba.druid.pool.DruidDataSource;
import com.yh.csx.bsf.core.util.ContextUtils;
import com.yh.csx.bsf.core.util.PropertyUtils;
import lombok.var;

import javax.sql.DataSource;

public class BigDataSource {

    public static DruidDataSource getDefaultDataSource(){
        var find = ContextUtils.getBean(DruidDataSource.class,"tidbDataSource",false);
        if(find==null)
        {
            find = ContextUtils.getBean(DruidDataSource.class,"clickHouseDataSource",false);
        }
        return find;
    }
}
