package com.yh.csx.bsf.cat.method;

import java.lang.annotation.*;

/**
 * @author huojuncheng
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CatMethod {
    String value() default "Method";
}
